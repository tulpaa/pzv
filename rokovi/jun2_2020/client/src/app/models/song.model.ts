export class Song{
    public _id : string = "";

    constructor(
        public artist : string,
        public title : string,
        public count : number
    ){}
}