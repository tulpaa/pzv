export class Step{
    public _id : string = "";
    public cilj : string = "";

    constructor(
        public redniBroj : number,
        public opis : string
    ){}
}